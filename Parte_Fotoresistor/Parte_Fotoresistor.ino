#include <LiquidCrystal.h>
#define COLS 16
#define ROWS 2
unsigned char bandera = 2;
float val = 0;
float tension;


void setup() {
 pinMode (0, OUTPUT);
 pinMode (8, OUTPUT);
 pinMode (7, OUTPUT);
 pinMode (13, INPUT);

 //Display
 pinMode ( 12,OUTPUT);
 pinMode ( 11,OUTPUT);
 pinMode ( 5,OUTPUT);
 pinMode ( 4,OUTPUT);
 pinMode ( 3,OUTPUT);
 pinMode ( 2,OUTPUT);
}

void loop()
{
val = analogRead (0);
tension = 0.0049 *val;

 if (digitalRead (13) == 1)
 {
  bandera = bandera + 1;
  Serial.println (bandera);
 }

 if (bandera == 1)
 {
  if (tension < 0.6)
  {
   digitalWrite (7, LOW);
   digitalWrite (8, LOW);
   lcd.clear ();
   lcd.setCursor (0,0);
   lcd.print ("ILUMATIC");
   lcd.setCursor (0,1);
   lcd.print ("Modo Diurno"); 
  }
  else
  {
    digitalWrite (7, HIGH);
    digitalWrite (8, HIGH);
    lcd.clear ();
    lcd.setCursor (0,0);
    lcd.print ("ILUMATIC");
    lcd.setCursor (0,1);
    lcd.print ("Modo Diurno");
  }
 }
 
  if (bandera ==2)
  {
    lcd.clear ();
    lcd.setCursor (0,0);
    lcd.print ("ILUMATIC");
    lcd.setCursor (0,1);
    lcd.print ("Apagado");
    digitalWrite (7, LOW);
    digitalWrite (8, LOW);
    bandera = 0;
  }

  delay (500);
}
