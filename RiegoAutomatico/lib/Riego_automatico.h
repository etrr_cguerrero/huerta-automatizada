#include <arduino.h>
#include <LiquidCrystal.h>

/**************************************
***DECLARO LAS VARIABLES A UTILIZAR***
**************************************/

//Botones
int bArriba = 1;
int bAbajo = 0;
int aceptar = 3;
int salir = 2;
int estado = 1;

//Fotoresistor
float fotoresistor;
float fotoresistorVolt;
float porcentaje_Luz = fotoresistorVolt * 0.5;


//Humedad
float humedad;
float humedadVolt;
float porcentaje_Humedad = humedadVolt * 0.5;

/**********************************
***Declaro lo que hace cada menu***
***********************************/

void LCDmenuA() //Estado 1
{
  lcd.setCursor(0, 0);
  lcd.print("-->Flores");
  lcd.setCursor(0, 1);
  lcd.print("Captus");

  if (digitalRead(bArriba) == HIGH)
  {
    estado = 1;
    lcd.clear();
  }
  if (digitalRead(bAbajo) == HIGH)
  {
    estado = 2;
    lcd.clear();
  }
  if (digitalRead (aceptar) == HIGH)
  {
    estado = 3;
    lcd.clear();
  }
}

void LCDmenuB() //Estado 2
{
  lcd.setCursor(0, 0);
  lcd.print("Flores");
  lcd.setCursor(0, 1);
  lcd.print("-->Captus");

  if (digitalRead(bArriba) == HIGH)
  {
    estado = 1;
    lcd.clear();
  }
  if (digitalRead(bAbajo) == HIGH)
  {
    estado= 2;
    lcd.clear();
  }
  if (digitalRead (aceptar) == HIGH)
  {
    estado = 4;
    lcd.clear();
  }
}

void LCDmenuFlores() //Estado 3
{
  lcd.setCursor(0, 0);
  printf("La humedad es de %d\n", porcentaje_Humedad);
  lcd.setCursor(0, 1);
  printf("La luz es de %d\n", porcentaje_Luz);
}

void LCDmenuCaptus() //Estado 4
{
  lcd.setCursor(0, 0);
  printf("La humedad es de %d\n", porcentaje_Humedad);
  lcd.setCursor(0, 1);
  printf("La luz es de %d\n", porcentaje_Luz);
}
