
#include <Wire.h>
#include <LiquidCrystal_I2C_AvrI2C.h>

LiquidCrystal_I2C_AvrI2C lcd(0x27, 20, 4);

byte boton1 = 3;
byte boton2 = 2;
byte boton3 = 9;
byte boton4 = 10;

byte estado = 1;

void setup() {

  lcd.begin();
  lcd.backlight();
  pinMode(boton1, INPUT);
  pinMode(boton2, INPUT);
  pinMode(boton3, INPUT);
  pinMode(boton4, INPUT);
  digitalWrite(boton1, HIGH);
  digitalWrite(boton2, HIGH);
  digitalWrite(boton3, HIGH);
  digitalWrite(boton4, HIGH);
}

void loop() {

  if (estado == 1) {
    do {
      LCDmenu0();  //Primero hace lo que dice el do
    } while (estado == 1); lcd.clear();
  }

  if (estado == 2) {
    do {
      LCDmenu1();
    } while (estado == 2);
    lcd.clear();
  }

  if (estado == 3) {
    do {
      LCDmenu2();
    } while (estado == 3);
    lcd.clear();
  }

  if (estado == 4) {
    do {
      //LCDmenu4();
    } while (estado == 4);
    lcd.clear();
  }

  if (estado == 5) {
    do {
      //LCDmenu5();
    } while (estado == 5);
    lcd.clear();
  }

  if (estado == 6) {
    do {
      //LCDmenu6();
    } while (estado == 6);
    lcd.clear();
  }

  if (estado == 7) {
    do {
     // LCDmenu7();
    } while (estado == 7);
    lcd.clear();
  }

  if (estado == 8) {
    do {
      //LCDmenu8();
    } while (estado == 8);
    lcd.clear();
  }

  if (estado == 9) {
    do {
      //LCDmenu9();
    } while (estado == 9);
    lcd.clear();
  }

}

void LCDmenu0()
{
  lcd.setCursor(0, 0);
  lcd.print("cafe");
  lcd.setCursor(0, 3);
  lcd.print("te");

  if (digitalRead(boton1) == HIGH)
  {
    estado = 2;   
    lcd.clear();
  }
  if (digitalRead(boton2) == HIGH)
  {
    estado= 3;   
    lcd.clear();
  }
}

void LCDmenu1()
{
  lcd.setCursor(0, 0); lcd.print("solo");
  lcd.setCursor(0, 3); lcd.print("mitad");
  lcd.setCursor(9, 0); lcd.print("nube");
  lcd.setCursor(15, 3); lcd.print("ATRAS");
  delay(500);

  if (digitalRead(boton1) == HIGH)
  {
    estado = 4;     
  }
  if (digitalRead(boton2) == HIGH)
  {
    estado = 5; 
  }
  if (digitalRead(boton3) == HIGH)
  {
    estado = 6; 
  }
  if (digitalRead(boton4) == HIGH)
  {
    estado = 1;   
  }
}

void LCDmenu2()
{
  lcd.setCursor(0, 0); lcd.print("negro");
  lcd.setCursor(0, 3); lcd.print("rojo");
  lcd.setCursor(9, 0); lcd.print("tila");
  lcd.setCursor(15, 3); lcd.print("ATRAS");
  delay(500);

  if (digitalRead(boton1) == HIGH)
  {
    estado = 7;     
  }
  if (digitalRead(boton2) == HIGH)
  {
    estado = 8; 
  }
  if (digitalRead(boton3) == HIGH)
  {
    estado = 9; 
  }
  if (digitalRead(boton4) == HIGH)
  {
    estado = 1;   
  }
}
