//Display
#include <LiquidCrystal.h>
const int rs = 7, en = 8, d4 = 9, d5 = 6, d6 = 12, d7 = 5;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

//Fotoresistor
#define COLS 16
#define ROWS 2

unsigned char bandera = 2;
float val = 0;
float tension;


//Humedad
const int rele = 10;
float humedad;
float humedadVolt;
float porcentaje;


void setup() {

   //Display
 pinMode ( 12,OUTPUT);
 pinMode ( 11,OUTPUT);
 pinMode ( 5,OUTPUT);
 pinMode ( 4,OUTPUT);
 pinMode ( 3,OUTPUT);
 pinMode ( 2,OUTPUT);
  lcd.begin(16, 2);
  Serial.begin(9600);
  pinMode (4,OUTPUT);
  pinMode (rele,OUTPUT);

   //Declaro pines del fotoresistor
 pinMode (0, OUTPUT);
 pinMode (8, OUTPUT);
 pinMode (7, OUTPUT);
 pinMode (13, INPUT);
 
}

void loop() {

  //MEDIR HUMEDAD
  lcd.setCursor(0,0);
  
  lcd.print("HUMEDAD y LUZ");
  humedad=analogRead(A4); //el ADC convierte a cuentas
  //Serial.println(humedad);

  /************************************
  AHora convierto las cuentas del ADC
  en tension
  Si 1023 cuentas son 5V
  entonces humedadVolt=humerdad*5/1023
  *************************************/
  
  humedadVolt=humedad*5/1023;
  Serial.println(humedadVolt);
  
  lcd.setCursor(0,1); // "Segundo renglon"

  porcentaje = humedadVolt*100*0.3030;
  
  if (humedadVolt == 0)
  {
    lcd.print ("Humedad 0%");
  }
  if (humedadVolt >= 1.8)
  {
    lcd.print ("Humedad 100%");
  }
  if((humedadVolt >= 0) && (humedadVolt <= 1.8))
  {
    lcd.print ("Porcentaje:");
    lcd.print (porcentaje);
  }
  if (humedadVolt >= 1.5)
  {
    digitalWrite (4,HIGH);
    digitalWrite (rele,HIGH);
  }
  else 
  {
    digitalWrite (4,LOW);
    digitalWrite (rele,LOW);
  }

//MEDICIÓN DEL FOTORESISTOR

val = analogRead (0);
tension = 0.0049 *val;

 if (digitalRead (13) == 1)
 {
  bandera = bandera + 1;
  Serial.println (bandera);
 }

 if (bandera == 1)
 {
  if (tension < 0.6)
  {
   digitalWrite (7, LOW);
   digitalWrite (8, LOW);
   lcd.clear ();
   lcd.setCursor (0,0);
   lcd.print ("ILUMATIC");
   lcd.setCursor (0,1);
   lcd.print ("Modo Diurno"); 
  }
  else
  {
    digitalWrite (7, HIGH);
    digitalWrite (8, HIGH);
    lcd.clear ();
    lcd.setCursor (0,0);
    lcd.print ("ILUMATIC");
    lcd.setCursor (0,1);
    lcd.print ("Modo Diurno");
  }
 }
 
  if (bandera ==2)
  {
    lcd.clear ();
    lcd.setCursor (0,0);
    lcd.print ("ILUMATIC");
    lcd.setCursor (0,1);
    lcd.print ("Apagado");
    digitalWrite (7, LOW);
    digitalWrite (8, LOW);
    bandera = 0;
  }

//lcd.print(humedadVolt);
  delay(500);
  lcd.clear();

  
}
